﻿using System;
using System.Collections.Generic;
using System.Text;
using GreetingsAPI.Contracts;
using GreetingsAPI.Controllers;
using GreetingsAPI.Models;
using Moq;
using NUnit.Framework;
namespace GreetinsAPI.UnitTest.Controllers
{
    [TestFixture]
   public class GreetingsControllerTests
    {
        [Test]
        public void GetGreetings_ShouldReturn_WelcomeMesage()
        {
            const string _message= "Welcome to BBC Studios TT";
            var mock = new Mock<IMessageHandler>();
            mock.Setup(x => x.DisplayMessgae(It.IsAny<string>())).Returns(new Greetings { Message= _message });
            var ctrl = new GreetingsController(mock.Object);
            var result = ctrl.Get(It.IsAny<string>());
            var resultUnderTest = (Microsoft.AspNetCore.Mvc.ObjectResult)result.Result;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsTrue(Convert.ToString(((Greetings)resultUnderTest.Value).Message) == _message);
            Assert.IsTrue(resultUnderTest.StatusCode==200);

        }
    }
}
