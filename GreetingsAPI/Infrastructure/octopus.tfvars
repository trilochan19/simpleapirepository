environment = "#{Octopus.Environment.ShortName}"

app_version = "#{Octopus.Release.Number}"

application = "#{Octopus.Project.Name}"

slot_name = "#{app_service_deployment_slot_name}"
