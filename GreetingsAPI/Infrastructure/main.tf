variable "environment" {}
variable "application" {}
variable "app_version" {}
variable "location" {}
variable "slot_name" {}

provider "azurerm" {}

module "resource_group" {
  source      = "git::ssh://git@bitbucket.org/bbcworldwide/terraform-modules.git//azure/modules/resource-group"
  environment = "${var.environment}"
  application = "${var.application}"
  app_version = "${var.app_version}"
  location    = "${var.location}"
}

# Create a billing plan to run the app inside - this could be dedicated to this app, or shared with other apps
module "app_service_plan" {
  source              = "git::ssh://git@bitbucket.org/bbcworldwide/terraform-modules.git//azure/modules/app-service-plan"
  environment         = "${var.environment}"
  application         = "${var.application}"
  resource_group_name = "${module.resource_group.name}"
  app_version         = "${var.app_version}"
  location            = "${module.resource_group.location}"

  sku {
    size     = "B1"
    capacity = 1
    tier     = "Basic"
  }
}

# create web site deployment infrastructure
module "app_service_api" {
  source              = "git::ssh://git@bitbucket.org/bbcworldwide/terraform-modules.git//azure/modules/app-service-with-ad-auth"
  environment         = "${var.environment}"
  application         = "${var.application}"
  resource_group_name = "${module.resource_group.name}"
  app_version         = "${var.app_version}"
  location            = "${module.resource_group.location}"
  app_service_plan_id = "${module.app_service_plan.id}"

  # An S1/Standard instance/sku would be required for slot names
  slot_name = "${var.slot_name}"
}
