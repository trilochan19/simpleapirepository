﻿using GreetingsAPI.Contracts;
using GreetingsAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace GreetingsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GreetingsController : ControllerBase
    {
        private readonly IMessageHandler _messageHandler;
        public GreetingsController(IMessageHandler messageHandler)
        {
            _messageHandler = messageHandler;
        }
        [HttpGet("{message}")]
        public ActionResult<Greetings> Get(string message)
        {
            return Ok(_messageHandler.DisplayMessgae(message));
        }
    }
}
