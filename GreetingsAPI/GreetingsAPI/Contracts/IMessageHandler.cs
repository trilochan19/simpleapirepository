﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreetingsAPI.Models;

namespace GreetingsAPI.Contracts
{
   public interface IMessageHandler
    {
        Greetings DisplayMessgae(string message);
    }
}
