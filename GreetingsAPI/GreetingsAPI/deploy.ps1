# Use this script to deploy the terraform resources manually using the az and terraform cli

# ensure you are logged in
az login

# Prep to deploy
terraform init
terraform plan -input=false

# Make sure you are ready to deploy!
terraform apply -input=false -auto-approve