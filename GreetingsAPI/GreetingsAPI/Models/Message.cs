﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreetingsAPI.Contracts;

namespace GreetingsAPI.Models
{
    public class Message : IMessageHandler
    {
        public Greetings DisplayMessgae(string message)
        {
            const string greetMessagePrefix = "Welcome to BBC Studios ";
            return new Greetings { Message = greetMessagePrefix + message };
        }
    }
}
