
$IsTeamcity   = If(Test-Path env:\TEAMCITY_VERSION) { $true } Else { $false }
$BuildNumber  = if (Test-Path env:\build_number) { $env:build_number } Else { "1.0.0.0" }
$outputFolder = "$PSScriptRoot\..\dist\"
$packageId    = "resource-group-example" 
$packagePath  = "$PSScriptRoot"
$ToolsPath    = "\tools\dotnet"

    Function Install-Octo() {
        $toolPath   = "$ToolsPath\dotnet-octo.exe"
        $lastUpdate = (Get-Item $toolPath -ErrorAction SilentlyContinue).LastWriteTime
        $command    = if($lastUpdate) { "update" } else { "install" }
        $needUpdate = $lastUpdate -and (((Get-Date) - $lastUpdate) -gt (New-timespan -days 30))

        if(($command -eq "install") -or $needUpdate) {
            # Install the dotnet octo tool CLI if not already available or older than 30 days
            dotnet tool $command Octopus.DotNet.Cli --tool-path $ToolsPath

            if($LastExitCode -ne 0) {
                throw "Error installing dotnet octo global tool - Exit code $LastExitCode"
            }
        }
        
        If (-Not ($env:PATH -match "$ToolsPath;")) {
            # Add the octo tools path to the PATH env variable
            $env:PATH = "$ToolsPath;" + $env:PATH
        }
    }

    Function Create-TerraformZipArtifact([String] $id, [String] $version, [String] $PathToZip, [String] $outputFolder) {
        # Clear the output folder
        Remove-Item -path $outputFolder -recurse -force -ErrorAction silentlycontinue

        # Pack the terraform files into a nupkg with version number from Teamcity
        Write-Host "Packing .tf .tfvar .json files from '$packagePath'"
        $result = & dotnet octo pack --id=$id --version=$version --basePath=$PathToZip --outFolder=$outputFolder --include=*.tf --include=*.tfvars --include=*.json --format=zip

        if($LastExitCode -ne 0) {
            throw "Error during pack. $result"
        } else {
             $result | Write-Host
        }
        # Return path to created package
        $zipPath = ((Get-ChildItem "$outputFolder\$id*" | Select-Object -First 1).FullName)

        Write-Host "Zipped terraform files to $ZipPath" -ForegroundColor Green

        return $ZipPath
    }

    Function Publish-TCArtifacts([String] $folder){
        If ($IsTeamcity -and (Test-Path $folder)) { 
            Write-Host "##teamcity[publishArtifacts '$folder']" 
        } else {
            Write-Host "Teamcity Artifact publish skipped" -ForegroundColor Yellow
        }
    }

    Function Publish-ToOctopusGallery ([String] $packagePath) {
        If (($IsTeamcity) `
        -and (Test-Path env:\Octopus_Url) `
        -and (Test-Path env:\Octopus_ApiKey)) {
            Write-Host "Publishing $packagePath to $env:Octopus_Url"
            dotnet octo push --package $packagePath --server $env:Octopus_Url --apiKey  $env:Octopus_ApiKey

            if($LastExitCode -ne 0) {
                throw "Error pushing package to Octopus"
            }
        }
        else {
            Write-Host "Octopus publish skipped" -ForegroundColor Yellow
        }
    }

Install-Octo

$package = Create-TerraformZipArtifact -id $packageId -version $BuildNumber -PathToZip $packagePath -outputFolder $outputFolder

# Publish the build artifacts in Teamcity
Publish-TCArtifacts $outputFolder

# Push the .nupkg to the octopus server if we have credentials to do so
Publish-ToOctopusGallery $package